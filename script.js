"use strict"

function addNumber (num) {
    $('#number').val($('#number').val() + num);
}

function toggleStart(){
    if($("img").hasClass("pause")){
        $("img").css("animation-play-state","running");
    }
    else{
        $("img").css("animation-play-state","paused");
    }

    $("img").toggleClass("pause");
}

$(document).ready( () => {
   
    let container = $(".container")[0];
    for(let i = 0; i < 10; i++){
        $('<img>',{
            src: "images/" + i + ".png",
            class: "img" + i,
            height: "50px",
            width: "50px",
            onclick: "addNumber('" + i + "')"
        }).appendTo(container);
    }

    $('<img>',{
        src: "images/cross.png",
        class: "cross",
        height: "50px",
        width: "50px",
        onclick: "toggleStart()"
    }).appendTo(container);
    
    $("img").addClass("pause");
});